const mongoose = require('mongoose');

const credentialSchema = mongoose.Schema({
  username: {
    type: String,
    uniq: true
  },
  password: String
});

module.exports.Credential = mongoose.model('Credential', credentialSchema);