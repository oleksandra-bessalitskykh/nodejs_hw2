const mongoose = require('mongoose');

const notesSchema = mongoose.Schema({
  userId: String,
  completed: Boolean,
  text: String,
  createdDate: {
    type: String,
    default: Date.now()
  }
});

module.exports.Note = mongoose.model('Note', notesSchema);