const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
  username: {
    type: String,
    uniq: true
  },
  password: String,
  createdDate: {
    type: String,
    default: Date.now()
  },
});

module.exports.User = mongoose.model('User', userSchema);