const express = require('express');
const notesRouter = express.Router();

const {asyncWrapper} = require('./helpers');
const {validateNote} = require('./middlewares/validationMiddleware');
const {getNotes, postNote, getNote, putNote, patchNote, deleteNote} = require('../controllers/notesController');

notesRouter.get('/', asyncWrapper(getNotes));

notesRouter.post('/', asyncWrapper(validateNote), asyncWrapper(postNote));

notesRouter.get('/:id', asyncWrapper(getNote));

notesRouter.put('/:id', asyncWrapper(validateNote), asyncWrapper(putNote));

notesRouter.patch('/:id', asyncWrapper(patchNote));

notesRouter.delete('/:id', asyncWrapper(deleteNote));

module.exports = notesRouter;