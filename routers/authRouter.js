const express = require('express');
const authRouter = express.Router();

const { asyncWrapper } = require('./helpers');
const { validateRegistration } = require('./middlewares/validationMiddleware');
const { login, registration } = require('../controllers/authController');

authRouter.post('/register', asyncWrapper(validateRegistration), asyncWrapper(registration));
authRouter.post('/login', asyncWrapper(login));

module.exports = authRouter;