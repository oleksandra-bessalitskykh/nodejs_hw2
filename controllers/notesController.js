const {Note} = require('../models/noteModel');

module.exports.getNotes = async (req, res) => {
  const {skip = 0, limit = 5} = req.query;
  const notes = await Note.find({userId: req.user._id}, [], { skip: parseInt(skip), limit: parseInt(limit) });
  res.json({notes});
};

module.exports.postNote = async (req, res) => {
  const note = new Note({
    userId: req.user._id,
    completed: false,
    text: req.body.text
  });
  await note.save();
  res.json({message: 'New note created successfully'});
};

module.exports.getNote = async (req, res) => {
  const note = await Note.findById(req.params.id);
  res.json({note});
};

module.exports.putNote = async (req, res) => {
  await Note.findByIdAndUpdate(req.params.id, {text: req.body.text});
  res.json({message: 'Note edited successfully'});
};

module.exports.patchNote = async (req, res) => {
  const note = await Note.findById(req.params.id);
  await Note.findByIdAndUpdate(req.params.id, {completed: !note.completed});
  res.json({message: 'Note edited successfully'});
};

module.exports.deleteNote = async (req, res) => {
  await Note.findByIdAndDelete(req.params.id);
  res.json({message: 'Note deleted successfully'});
};
