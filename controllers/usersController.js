const bcrypt = require('bcrypt');

const {User} = require('../models/userModel');
const {Credential} = require('../models/credentialsModel');

module.exports.getUser = async (req, res) => {
  const user = await User.findById(req.user._id);
  res.json({user});
};

module.exports.deleteUser = async (req, res) => {
  await User.findByIdAndDelete(req.user._id);
  await Credential.findOneAndDelete({username: req.user.username});
  res.json({message: 'User deleted successfully'});
};

module.exports.patchUser = async (req, res) => {
  const {oldPassword, newPassword} = req.body;
  const credentials = await Credential.findOne({username: req.user.username});
  if (!(await bcrypt.compare(oldPassword, credentials.password))) {
    return res.status(400).json({message: `Wrong password!`});
  }
  await Credential.findOneAndUpdate({username: req.user.username}, {
    password: await bcrypt.hash(newPassword, 10),
    new: true
  });
  res.json({message: 'Success'});
};