const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const {JWT_SECRET} = require('../config');
const {User} = require('../models/userModel');
const {Credential} = require('../models/credentialsModel');

module.exports.registration = async (req, res) => {
  const {username, password} = req.body;

  const credentials = new Credential({
    username,
    password: await bcrypt.hash(password, 10)
  });

  const user = new User({
    username
  });

  await credentials.save();

  await user.save();

  res.json({message: 'Success'});
};

module.exports.login = async (req, res) => {
  const {username, password} = req.body;

  const credentials = await Credential.findOne({username});
  const user = await User.findOne({username});

  if (!credentials) {
    return res.status(400).json({message: `No user with username '${username}' found!`});
  }

  if (!(await bcrypt.compare(password, credentials.password))) {
    return res.status(400).json({message: `Wrong password!`});
  }

  const token = jwt.sign({username: user.username, _id: user._id}, JWT_SECRET);
  res.json({message: 'Success', jwt_token: token});
};